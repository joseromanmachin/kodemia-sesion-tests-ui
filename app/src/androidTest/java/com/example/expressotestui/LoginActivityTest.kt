package com.example.expressotestui


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class LoginActivityTest {

    private val username = "chike"
    private val password = "123"


    @get:Rule
    val activityRule = ActivityTestRule(LoginActivity::class.java)

    @Test
    fun clickLoginButton_opensLoginUi() {

        //editext user
        onView(withId(R.id.editTextTextPersonName))
            .perform(ViewActions.typeText(username))

        //editext password
        onView(withId(R.id.editTextNumberPassword)).
        perform(ViewActions.typeText(password))

        //button
        onView(withId(R.id.button)).
        perform(click())

     //   Espresso.onView(withId(R.id.tv_login))
       //     .check(matches(withText("Success")))
    }
}