package com.example.expressotestui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.widget.Toast
import com.example.expressotestui.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {
    lateinit var binding : ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.apply {
            button.setOnClickListener {
                Toast.makeText(this@LoginActivity,"testeando",Toast.LENGTH_SHORT).show()
                val intent = Intent(applicationContext,MainActivity::class.java)
                startActivity(intent)
            }



        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN){

            print("accion down")
        }
        if (event?.action == MotionEvent.ACTION_UP){
            print("accion up")
        }
        return super.onTouchEvent(event)
    }
}